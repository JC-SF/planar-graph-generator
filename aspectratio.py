import math


def define_dimensions(nodes, density, ratio): 
    # nodes = 100
    # density = 0.00005
    # ratio=0.5 

    area = nodes/density
    width_squared = area/ratio
    width = math.sqrt(width_squared)
    height = ratio*width

    height = height//1
    width = width//1

    return {"height":height, "width":width}

print(define_dimensions(2000, 0.00005, 0.5))

